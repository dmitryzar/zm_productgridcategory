<?php

/**
 * ZMage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   ZM
 * @package    ZM_ProductGridCategory
 * @copyright  Copyright (c) 2015 ZMage (http://zmage.org)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     ZMage
 */

class ZM_ProductGridCategory_Model_Observer
{
	
	public function addToProductGrid(Varien_Event_Observer $observer)
	{	
		$block = $observer->getEvent()->getBlock();
		
		if (!isset($block)) return;

        switch ($block->getType()) {
            case 'adminhtml/catalog_product_grid':
				$block->addColumnAfter('zm_category_list', array(
					'header'	=> Mage::helper('zm_productgridcategory')->__('Category'),
					'index'		=> 'zm_category_list',
					'sortable'	=> false,
					'width' => '250px',
					'type'  => 'options',
					'options'	=> Mage::getSingleton('zm_productgridcategory/system_config_source_category')->toOptionArray(),
					'renderer'	=> 'zm_productgridcategory/catalog_product_grid_render_category',
					'filter_condition_callback' => array($this, 'filterCallback'),
				),'name');
				
				$block->addColumnAfter('zm_price', array(
					'header'=> Mage::helper('catalog')->__('Special price'),
					'type'  => 'price',
					'currency_code' => Mage::app()->getStore()->getBaseCurrency()->getCode(),
					'index' => 'special_price',
					'renderer'	=> 'zm_productgridcategory/catalog_product_grid_render_price',
				), 'price');
			break;
        }
	}
	
	public function filterCallback($collection, $column)
	{
		$value = $column->getFilter()->getValue();
		$_category = Mage::getModel('catalog/category')->load($value);
		$collection->addCategoryFilter($_category);
		
		return $collection;
	}
	
	
}
