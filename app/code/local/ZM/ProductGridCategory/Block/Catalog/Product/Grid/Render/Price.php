<?php

/**
 * ZMage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   ZM
 * @package    ZM_ProductGridCategory
 * @copyright  Copyright (c) 2015 ZMage (http://zmage.org)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     ZMage
 */

 class ZM_ProductGridCategory_Block_Catalog_Product_Grid_Render_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $product = Mage::getModel('catalog/product')->load($row->getEntityId());
        $p_price= $product->getPrice();
		
		
		if ($s_price = $product->getSpecialPrice()) {
			$perc = round(100-(($s_price/$p_price)*100));
			$c_date = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
			$s_price_date = $product->getSpecialToDate();
			
			$price_html = Mage::helper('core')->currency($s_price, true, false).'&nbsp;<b>(-'.$perc.'%)</b><br />';
			
			if ($s_price_date) {
				$date_format = new Zend_Date($s_price_date);
				$price_html .= '<span style="font-size:smaller";>'.$this->__('after').' '.$date_format->toString('dd MMM yyyy').'</span>';
			} else {
				$price_html .= '<span style="font-size:smaller";>'.$this->__('Not time limit').'</span>';
			}	
		} else {
			$price_html = '-';
		}
	
        return  $price_html;
    }

}
